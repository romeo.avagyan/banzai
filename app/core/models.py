from django.db import models


class Contact(models.Model):
    username = models.CharField(max_length=30)
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    created_on = models.DateTimeField(auto_now_add=True)