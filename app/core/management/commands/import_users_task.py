# TODO:: Was coding on vi, this file needs linting and pep8
import os
import shutil
import tempfile
from os import name
from re import template
import datetime

import boto3
from moto import mock_s3

from celery_progress.backend import ProgressRecorder
from app import celery_app
from django.conf import settings
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse
from django.utils import timezone
from django.db.models import Q
from openpyxl import load_workbook

from ...models import Contact

class BaseTask(celery_app.Task):
    ignore_result = False

    def __call__(self, *args, **kwargs):
        print("Starting %s" % self.name)
        return self.run(*args, **kwargs)

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        # exit point of the task whatever is the state
        print("End of %s" % self.name)

class ImportUserFromExcelTask(BaseTask):
    name = "ImportUserFromExcelTask"

    def insert_into_row(self, row: dict) -> User:
        username=row.get("username")
        email=row.get("email")

        duplicate_row = Contact.objects.filter(Q(username=username) | Q(email=email)).latest('created_on')
        
        now = timezone.now()
        if (now - duplicate_row.created_on) >= datetime.timedelta(minutes=3):
            number = row.get("phone")

            if number:
                contact = Contact.objects.create(username=username, phone=number, email=email)
                return contact

    def save_file_to_s3(self, bucket_name, file_path):
        try:
            s3 = boto3.client('s3', region_name='us-east-1')
            # Using file_path as a s3 object name is not the best idea...
            # But as we discussed :D This is not a real project
            s3.upload_file(file_path, bucket_name, file_path)
        except Exception as e:
            # We need log here
            print("exception raised while uploading object e: %s", e)

    @mock_s3
    def mock_save_file_to_s3(self, file_path):
        conn = boto3.resource('s3', region_name='us-east-1')
        
        # We need to create the bucket since this is all in Moto's 'virtual' AWS account.
        bucket_name = 'this_is_sparta'
        conn.create_bucket(Bucket=bucket_name)

        self.save_file_to_s3(bucket_name, file_path)

    def run(self, file_path, *args, **kwargs):
        progress_recorder = ProgressRecorder(self)

        self.mock_save_file_to_s3(file_path=file_path)

        wb = load_workbook(file_path)
        sheet = wb.worksheets[0]
        total_record = sheet.max_row

        index = 0

        for row in sheet.iter_rows(values_only=True):
            contact = {
                "username": row[0],
                "phone": row[1],
                "email": row[2]
            }
            self.insert_into_row(contact)
            index += 1
            progress_recorder.set_progress(index, total=total_record, description="Inserting row into table")
            print("Inserting row %s into table" % index)

        return {
            "detail": "Successfully import user"
        }


@celery_app.task(bind=True, base=ImportUserFromExcelTask)
def import_users_task(self, *args, **kwargs):
    return super(type(self), self).run(*args, **kwargs)