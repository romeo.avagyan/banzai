from django.urls import path

from . import views

app_name = 'core'

urlpatterns = [
    path("", views.IndexTemplateView.as_view(), name="index"),
    path("import-user", views.import_user_view, name="import"),
    path("celery-progress", views.get_progress_view, name="progress"),
]