import os
import json

from django.shortcuts import render
from django.conf import settings
from django.views.generic import TemplateView
from django.http import Http404, HttpResponse

from celery_progress.backend import Progress

from .forms import ImportFileForm
from .management.commands import import_users_task
from .utils import in_memory_file_to_temp


# Create your views here.

class IndexTemplateView(TemplateView):
    template_name = "index.html"

def import_user_view(request):
    """
    The column of the excel file should be part of
    [Username, Password, Email, First Name, Last Name]
    """
    form = ImportFileForm(request.POST, request.FILES)
    if form.is_valid():
        filepath = os.path.join(
            settings.MEDIA_ROOT, in_memory_file_to_temp(form.cleaned_data.get('document_file'))
        )
        task = import_users_task.delay(os.path.join(settings.MEDIA_ROOT, filepath))
        return HttpResponse(json.dumps({"task_id": task.id}), content_type='application/json')
    raise Http404

def get_progress_view(request):
    progress = Progress(request.GET.get("task_id"))
    return HttpResponse(json.dumps(progress.get_info()), content_type='application/json')