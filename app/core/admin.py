from django.contrib import admin
from .models import Contact

class ContactAdmin(admin.ModelAdmin):
    readonly_fields = ('created_on',)

admin.site.register(Contact,ContactAdmin)
