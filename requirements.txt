Django>=2.2
django-extensions>=3.1
celery==4.4.1
redis==3.4.1
psycopg2>=2.7.5,<2.8.0
celery-progress==0.0.12
django-cors-headers
openpyxl
cryptography==2.8 # I know this version is too old
boto3
moto[s3]